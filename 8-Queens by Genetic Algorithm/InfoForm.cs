﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _8_Queens_by_Genetic_Algorithm
{
    public partial class InfoForm : Form
    {
        public InfoForm()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Enabled = true;
        }

        private void InfoForm_Load(object sender, EventArgs e)
        {
            rchtxtAphorism.Text = "با سلام خدمت کاربران\n\n" +
                                  "در این برنامه سعی شده است که مسئله‌ی 8 وزیر به کمک الگوریتم ژنتیک حل شود.\n" +
                                  "اکثر ایرادهای منطقی و خطاهای رایج این برنامه گرفته شده است.\n\n" +
                                  "با تشکر\n" +
                                  "Mehdi MzIT";

            rchtxtInTheFuture.Text = "این برنامه شامل دو قسمت می‌باشد:\n" +
                                     "ایجاد یک فرآیند جدید در کنار نمایش فرآیند‌های قبلی.\n\n" +
                                     "در بخش ایجاد، می توانید موارد زیر را تعیین کنید:\n" +
                                     "۱. تعداد کروموزوم‌های اولیه\n" +
                                     "۲. حداکثر تعداد تولید فرزندان\n" +
                                     "۳. درصد ترکیب والدها\n" +
                                     "۴. درصد جهش‌ها\n\n" +
                                     "با فشار دادن دکمه‌ی نتیجه، محاسبه را در قسمت نتیجه نهایی مشاهده نمایید.";
        }
    }
}

