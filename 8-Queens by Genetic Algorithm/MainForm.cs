﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using _8_Queens_by_Genetic_Algorithm.Properties;
using ECP.PersianMessageBox;
using System.Data.SQLite;
using System.IO;

namespace _8_Queens_by_Genetic_Algorithm
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        SQLiteConnection con = new SQLiteConnection("Data Source=" + Application.StartupPath +
                                                   @"\DataBase\nQueensGenetic.db; " +
                                                    "Version=3;");

        //----------

        #region MainForm
        //----------

        #region FormDragwithMouse

        private void frmDragForm_Paint(object sender, PaintEventArgs e)
        {
            //Draws a border to make the Form stand out
            //Just done for appearance, not necessary

            Pen p = new Pen(Color.Gray, 3);
            e.Graphics.DrawRectangle(p, 0, 0, this.Width - 1, this.Height - 1);
            p.Dispose();
        }

        Point lastClick; //Holds where the Form was clicked

        private void frmDragForm_MouseDown(object sender, MouseEventArgs e)
        {
            lastClick = new Point(e.X, e.Y); //We'll need this for when the Form starts to move
        }

        private void frmDragForm_MouseMove(object sender, MouseEventArgs e)
        {
            //Point newLocation = new Point(e.X - lastE.X, e.Y - lastE.Y);
            if (e.Button == MouseButtons.Left) //Only when mouse is clicked
            {
                //Move the Form the same difference the mouse cursor moved;
                this.Left += e.X - lastClick.X;
                this.Top += e.Y - lastClick.Y;
            }
        }

        //If the user clicks on the Objects(on the form) we want the same kind of behavior
        //so we just call the Forms corresponding methods
        private void frmDragObjects_MouseDown(object sender, MouseEventArgs e)
        {
            frmDragForm_MouseDown(sender, e);
        }

        private void frmDragObjects_MouseMove(object sender, MouseEventArgs e)
        {
            frmDragForm_MouseMove(sender, e);
        }

        #endregion

        ////----------

        #region toolStripMenu

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (PersianMessageBox.Show("آیا می‌خواهید خارج شوید؟ ", "خروج", PersianMessageBox.Buttons.YesNo, PersianMessageBox.Icon.Question, PersianMessageBox.DefaultButton.Button2) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnMinimized_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            Form Info = new InfoForm();
            Info.Show();
            MPG_WinShutDowm.ShowShutForm sf = new MPG_WinShutDowm.ShowShutForm(new InfoForm());
            Info.Close();
        }

        #endregion

        ////----------

        #region toolStripLogo

        private void toolStripLogo_MouseHover(object sender, EventArgs e)
        {
            this.toolStripLogo.Cursor = System.Windows.Forms.Cursors.SizeAll;
        }

        #endregion

        //----------
        #endregion MainForm

        //----------

        #region Genetic Algorithm

        private void Fitness(int[,] x)
        {
            int Fit = 0;

            for (int i = 0; i < Settings.Default.NumberChromosomes; i++)
            {
                Fit = 0;
                for (int j = 0; j < Settings.Default.nQueens; j++)
                {
                    for (int t = 0; t < j; t++)
                    {
                        if (Math.Abs(j - t) == Math.Abs(x[i, j]) - x[i, t])
                        {
                            Fit++;
                        }
                    }
                    for (int t = j + 1; t < Settings.Default.nQueens; t++)
                    {
                        if (x[i, j] == x[i, t])
                        {
                            Fit++;
                        }
                        if (Math.Abs(j - t) == Math.Abs(x[i, j]) - x[i, t])
                        {
                            Fit++;
                        }
                    }
                }
                x[i, Settings.Default.nQueens] = Fit;
            }
        }

        private void Sort(int[,] x)
        {
            int temp;

            for (int i = 0; i < (Settings.Default.NumberChromosomes - 1); i++)
            {
                for (int j = (i + 1); j < Settings.Default.NumberChromosomes; j++)
                {
                    if (x[i, Settings.Default.nQueens] > x[j, Settings.Default.nQueens])
                    {
                        for (int k = 0; k <= Settings.Default.nQueens; k++)
                        {
                            temp = x[i, k];
                            x[i, k] = x[j, k];
                            x[j, k] = temp;
                        }
                    }
                }
            }
        }

        private void CrossOver(int[,] currentGeneration, int[,] newGeneration)
        {
            for (int i = 0; i < (Settings.Default.NumberChromosomes / 2); i++)
            {
                for (int j = 0; j <= Settings.Default.nQueens; j++)
                {
                    newGeneration[i, j] = currentGeneration[i, j];
                }
            }

            for (int i = (Settings.Default.NumberChromosomes / 2); i < Settings.Default.NumberChromosomes - 1; i += 2)
            {
                for (int j = 0; j <= Settings.Default.nQueens; j++)
                {
                    if (j < ((Settings.Default.PercentCrossOver / 100) * Settings.Default.nQueens))
                    {
                        newGeneration[i, j] = currentGeneration[i, j];
                        newGeneration[i + 1, j] = currentGeneration[i + 1, j];
                    }
                    else
                    {
                        newGeneration[i, j] = currentGeneration[i + 1, j];
                        newGeneration[i + 1, j] = currentGeneration[i, j];
                    }
                }
            }
        }

        private void Mutation(int[,] x)
        {
            Random r = new Random();
            for (int i = 0; i < Settings.Default.NumberChromosomes; i++)
            {
                if (r.Next(0, 100) >= Settings.Default.PercentMutation)
                {
                    x[i, r.Next(0, Settings.Default.nQueens)] = r.Next(1, Settings.Default.nQueens + 1);
                }
            }
        }

        private void CreateBaseChromosomes(int[,] currentGeneration, int[,] newGeneration)
        {
            Random r = new Random();
            for (int i = 0; i < Settings.Default.NumberChromosomes; i++)
            {
                for (int j = 0; j < Settings.Default.nQueens; j++)
                {
                    newGeneration[i, j] = 2;
                    currentGeneration[i, j] = r.Next(1, Settings.Default.nQueens + 1);
                }
            }

            Fitness(currentGeneration);
            Sort(currentGeneration);

            Fitness(newGeneration);
            Sort(newGeneration);
        }

        private void RunGeneticAlgorithm()
        {
            int[,] currentGeneration = new int[Settings.Default.NumberChromosomes, Settings.Default.nQueens + 1];
            int[,] newGeneration = new int[Settings.Default.NumberChromosomes, Settings.Default.nQueens + 1];

            Settings.Default.nRunGA = 0;
            Settings.Default.ResultValue = 0;
            CreateBaseChromosomes(currentGeneration, newGeneration);

            while ((currentGeneration[0, Settings.Default.nQueens] != 0) && (Settings.Default.nRunGA < Settings.Default.LimitedCrossOver))
            {
                CrossOver(currentGeneration, newGeneration);
                Mutation(newGeneration);
                Fitness(newGeneration);
                Sort(newGeneration);

                currentGeneration = newGeneration;
                Settings.Default.nRunGA++;
            }

            if (currentGeneration[0, Settings.Default.nQueens] == 0)
            {
                Settings.Default.ResultValue = 1;
            }

            string s = "";
            for (int i = 0; i < Settings.Default.nQueens; i++)
            {
                s += currentGeneration[0, i];
            }
            Settings.Default.ResultFinal = s;
        }

        #endregion Genetic Algorithm

        //----------

        #region ProgramDesign

        #region TransactionType

        private void LoadTransactionType()
        {
            rdbtnCreate.Checked = false;
            rdbtnCreate.Enabled = true;
            rdbtnShow.Checked = false;
            rdbtnShow.Enabled = true;
            btnCancelShow.Visible = false;
            cmbTreeSelectionGene.Visible = false;

            panelDetailsProblem.Visible = false;

            panelResultValueIcon.Visible = false;
        }

        private void rdbtnCreate_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbtnCreate.Checked == true)
            {
                rdbtnShow.Enabled = false;
                panelDetailsProblem.Visible = true;
                cmbTreeSelectionGene.Visible = false;

                IsReadOnlyDetailsProblem(false);
                btnCancelCreate.Text = "انصراف";
            }
        }

        private void rdbtnShow_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbtnShow.Checked == true)
            {
                rdbtnCreate.Enabled = false;
                panelDetailsProblem.Visible = false;
                cmbTreeSelectionGene.Visible = true;

                IsReadOnlyDetailsProblem(true);
                btnCancelCreate.Text = "حذف";
                LoadSelectionGene();
            }
        }

        private void btnCancelShow_Click(object sender, EventArgs e)
        {
            LoadTransactionType();
            LoadExecuteFinal();
        }

        private void LoadSelectionGene()
        {
            Settings.Default.VerificationSelectedIndexChanged = false;
            cmbTreeSelectionGene.Nodes.Clear();

            string Q_QueensLoad = "SELECT * FROM nQueens" +
                                  " ASC ORDER BY ID";

            SQLiteCommand Loadcom = new SQLiteCommand(Q_QueensLoad, con);

            con.Open();
            SQLiteDataReader Queensdr = Loadcom.ExecuteReader();
            while (Queensdr.Read())
            {
                DevComponents.AdvTree.Node eachID = new DevComponents.AdvTree.Node(Queensdr["ID"].ToString());
                cmbTreeSelectionGene.Nodes.Add(eachID);

                eachID.Name = Queensdr["ID"].ToString();
                eachID.Text = "فرآیند " + Queensdr["ID"].ToString();
                eachID.ImageIndex = Int32.Parse(Queensdr["ResultValue"].ToString());
            }
            con.Close();

            Settings.Default.VerificationSelectedIndexChanged = true;
        }

        private void LoadSelectedGeneID()
        {
            string Q_QueensSelected = "SELECT * FROM nQueens" +
                                      " WHERE ID=" + cmbTreeSelectionGene.SelectedNode.Name + "";

            SQLiteCommand Selectedcom = new SQLiteCommand(Q_QueensSelected, con);

            con.Open();
            SQLiteDataReader Queensdr = Selectedcom.ExecuteReader();
            while (Queensdr.Read())
            {
                Settings.Default.NumberChromosomes = Int32.Parse(Queensdr["NumberChromosomes"].ToString());
                Settings.Default.LimitedCrossOver = Int32.Parse(Queensdr["LimitedCrossOver"].ToString());
                Settings.Default.PercentCrossOver = Int32.Parse(Queensdr["PercentCrossOver"].ToString());
                Settings.Default.PercentMutation = Int32.Parse(Queensdr["PercentMutation"].ToString());
                Settings.Default.nRunGA = Int32.Parse(Queensdr["nRunGA"].ToString());
                Settings.Default.ResultValue = Int32.Parse(Queensdr["ResultValue"].ToString());
                Settings.Default.ResultFinal = Queensdr["ResultFinal"].ToString();
            }
            con.Close();
        }

        private void cmbTreeSelectionGene_SelectionChanged(object sender, DevComponents.AdvTree.AdvTreeNodeEventArgs e)
        {
            if (rdbtnShow.Checked == true)
            {
                if (Settings.Default.VerificationSelectedIndexChanged == true)
                {
                    panelDetailsProblem.Visible = false;

                    LoadSelectedGeneID();
                    LoadDetailsProblem();

                    panelDetailsProblem.Visible = true;
                }
            }
        }

        #endregion TransactionType

        //----------

        #region DetailsProblem

        private void LoadDetailsProblem()
        {
            intNumberChromosomes.Value = Settings.Default.NumberChromosomes;
            intLimitedCrossOver.Value = Settings.Default.LimitedCrossOver;
            intPercentCrossOver.Value = Settings.Default.PercentCrossOver;
            intPercentMutation.Value = Settings.Default.PercentMutation;
        }

        private void IsReadOnlyDetailsProblem(bool isbool)
        {
            bool notbool;
            if (isbool == true)
            {
                notbool = false;
            }
            else
            {
                notbool = true;
            }

            intNumberChromosomes.IsInputReadOnly = isbool;
            intNumberChromosomes.ButtonClear.Visible = notbool;
            intNumberChromosomes.ShowUpDown = notbool;

            intLimitedCrossOver.IsInputReadOnly = isbool;
            intLimitedCrossOver.ButtonClear.Visible = notbool;
            intLimitedCrossOver.ShowUpDown = notbool;

            intPercentCrossOver.IsInputReadOnly = isbool;
            intPercentCrossOver.ButtonClear.Visible = notbool;
            intPercentCrossOver.ShowUpDown = notbool;

            intPercentMutation.IsInputReadOnly = isbool;
            intPercentMutation.ButtonClear.Visible = notbool;
            intPercentMutation.ShowUpDown = notbool;

            btnCancelShow.Visible = isbool;
        }

        private void intNumberChromosomes_ValueChanged(object sender, EventArgs e)
        {
            Settings.Default.NumberChromosomes = intNumberChromosomes.Value;
        }

        private void intLimitedCrossOver_ValueChanged(object sender, EventArgs e)
        {
            Settings.Default.LimitedCrossOver = intLimitedCrossOver.Value;
        }

        private void intPercentCrossOver_ValueChanged(object sender, EventArgs e)
        {
            Settings.Default.PercentCrossOver = intPercentCrossOver.Value;
        }

        private void intPercentMutation_ValueChanged(object sender, EventArgs e)
        {
            Settings.Default.PercentMutation = intPercentMutation.Value;
        }

        private bool ErrorRecordNewGene()
        {
            bool isError = false;

            Settings.Default.ResultFinal = " قسمت‌های زیر را تعیین کنید:        \n";

            if (intNumberChromosomes.Value == 0)
            {
                Settings.Default.ResultFinal += "\n  تعداد کروموزوم‌های اولیه ";
                isError = true;
            }
            if (intLimitedCrossOver.Value == 0)
            {
                Settings.Default.ResultFinal += "\n  حداکثر تعداد تولید فرزندان ";
                isError = true;
            }
            if (intPercentCrossOver.Value == 0)
            {
                Settings.Default.ResultFinal += "\n  درصد ترکیب والدها ";
                isError = true;
            }
            if (intNumberChromosomes.Value == 0)
            {
                Settings.Default.ResultFinal += "\n  درصد جهش‌ها ";
                isError = true;
            }

            Settings.Default.ResultFinal += "\n\n";

            return isError;
        }

        private void InsertCom()
        {
            string Q_Insert = "INSERT INTO nQueens (NumberChromosomes, " +
                                                   "LimitedCrossOver, " +
                                                   "PercentCrossOver, " +
                                                   "PercentMutation, " +
                                                   "nRunGA, " +
                                                   "ResultValue, " +
                                                   "ResultFinal)" +

                                          " VALUES (@NumberChromosomes, " +
                                                   "@LimitedCrossOver, " +
                                                   "@PercentCrossOver, " +
                                                   "@PercentMutation, " +
                                                   "@nRunGA, " +
                                                   "@ResultValue, " +
                                                   "@ResultFinal);";

            SQLiteCommand Insertcom = new SQLiteCommand(Q_Insert, con);

            Insertcom.Parameters.AddWithValue("@NumberChromosomes", Settings.Default.NumberChromosomes);
            Insertcom.Parameters.AddWithValue("@LimitedCrossOver", Settings.Default.LimitedCrossOver);
            Insertcom.Parameters.AddWithValue("@PercentCrossOver", Settings.Default.PercentCrossOver);
            Insertcom.Parameters.AddWithValue("@PercentMutation", Settings.Default.PercentMutation);
            Insertcom.Parameters.AddWithValue("@nRunGA", Settings.Default.nRunGA);
            Insertcom.Parameters.AddWithValue("@ResultValue", Settings.Default.ResultValue);
            Insertcom.Parameters.AddWithValue("@ResultFinal", Settings.Default.ResultFinal);

            con.Open();
            Insertcom.ExecuteNonQuery();
            con.Close();
        }

        private void btnResultNewGene_Click(object sender, EventArgs e)
        {
            if (rdbtnCreate.Checked == true)
            {
                if (ErrorRecordNewGene() == false)
                {
                    if (PersianMessageBox.Show(" آیا می‌خواهید فرآیند‌ جدیدی ثبت کنید؟    ",
                                                   "ثبت فرآیند جدید",
                                                   PersianMessageBox.Buttons.YesNo,
                                                   PersianMessageBox.Icon.Question,
                                                   PersianMessageBox.DefaultButton.Button1) == DialogResult.Yes)
                    {
                        this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                        RunGeneticAlgorithm();
                        InsertCom();
                        ResultGene();
                        LoadChessPage();

                        this.Cursor = System.Windows.Forms.Cursors.Default;

                        PersianMessageBox.Show(Settings.Default.MessageRecord,
                                               "جزییات فرآیند جدید",
                                               PersianMessageBox.Buttons.OK,
                                               PersianMessageBox.Icon.Information,
                                               PersianMessageBox.DefaultButton.Button1);
                    }

                    btnCancelCreate.Text = "اتمام";
                }

                else
                {
                    PersianMessageBox.Show(Settings.Default.MessageRecord,
                                           "خطا",
                                           PersianMessageBox.Buttons.OK,
                                           PersianMessageBox.Icon.Error,
                                           PersianMessageBox.DefaultButton.Button1);
                }
            }
            else if (rdbtnShow.Checked == true)
            {
                LoadChessPage();
                ResultGene();
            }
        }

        private void btnCancelCreate_Click(object sender, EventArgs e)
        {
            if (rdbtnCreate.Checked == true)
            {
                LoadTransactionType();
                LoadExecuteFinal();
            }
            else if (rdbtnShow.Checked == true)
            {
                string id = cmbTreeSelectionGene.SelectedNode.Name;

                if (PersianMessageBox.Show(" آیا می‌خواهید فرآیند‌ " + id + " را حذف کنید؟    ",
                                                   "حذف فرآیند",
                                                   PersianMessageBox.Buttons.YesNo,
                                                   PersianMessageBox.Icon.Question,
                                                   PersianMessageBox.DefaultButton.Button2) == DialogResult.Yes)
                {
                    string Q_DeleteID = "DELETE FROM nQueens" +
                                        " WHERE ID=" + id + ";";

                    SQLiteCommand Deletecom = new SQLiteCommand(Q_DeleteID, con);

                    con.Open();
                    Deletecom.ExecuteNonQuery();
                    con.Close();

                    PersianMessageBox.Show(" فرآیند " + id + " حذف شد.    ",
                                           "حذف فرآیند",
                                           PersianMessageBox.Buttons.OK,
                                           PersianMessageBox.Icon.Information,
                                           PersianMessageBox.DefaultButton.Button1);

                    LoadSelectionGene();
                }
            }
        }

        #endregion DetailsProblem

        //----------

        #region ExecuteFinal

        private void LoadExecuteFinal()
        {
            panelResultValueIcon.Visible = false;
            lblnRunGA.Text = "";
            lblResultFinal.Text = "";

            tlpColumns.Controls.Add(this.panelQueen1, 1, 0);
            tlpColumns.Controls.Add(this.panelQueen2, 2, 0);
            tlpColumns.Controls.Add(this.panelQueen3, 3, 0);
            tlpColumns.Controls.Add(this.panelQueen4, 4, 0);
            tlpColumns.Controls.Add(this.panelQueen5, 5, 0);
            tlpColumns.Controls.Add(this.panelQueen6, 6, 0);
            tlpColumns.Controls.Add(this.panelQueen7, 7, 0);
            tlpColumns.Controls.Add(this.panelQueen8, 8, 0);
        }

        private void LoadChessPage()
        {
            int x = Int32.Parse(Settings.Default.ResultFinal);
            int[] rf = new int[Settings.Default.nQueens];

            for (int i = Settings.Default.nQueens; i > 0; i--)
            {
                rf[i - 1] = x % 10;
                x /= 10;
            }

            tlpChessPage.Controls.Add(panelQueen1, 1, rf[0]);
            tlpChessPage.Controls.Add(panelQueen2, 2, rf[1]);
            tlpChessPage.Controls.Add(panelQueen3, 3, rf[2]);
            tlpChessPage.Controls.Add(panelQueen4, 4, rf[3]);
            tlpChessPage.Controls.Add(panelQueen5, 5, rf[4]);
            tlpChessPage.Controls.Add(panelQueen6, 6, rf[5]);
            tlpChessPage.Controls.Add(panelQueen7, 7, rf[6]);
            tlpChessPage.Controls.Add(panelQueen8, 8, rf[7]);
        }

        private void ResultGene()
        {
            if (Settings.Default.ResultValue == 1)
            {
                panelResultValueIcon.BackgroundImage = global::_8_Queens_by_Genetic_Algorithm.Properties.Resources.ResultValueTrue;

                Settings.Default.MessageRecord = " فرآیند‌ جدید ثبت شد.    " +
                                                 "\n\n مسئله در مرحله " + Settings.Default.nRunGA + " به جواب رسید. ";
            }
            else
            {
                panelResultValueIcon.BackgroundImage = global::_8_Queens_by_Genetic_Algorithm.Properties.Resources.ResultValueFalse;

                Settings.Default.MessageRecord = " فرآیند‌ جدید ثبت شد.    " +
                                                 "\n\n مسئله در " + Settings.Default.nRunGA + " مرحله به جواب نرسید. ";
            }
            panelResultValueIcon.Visible = true;

            lblnRunGA.Text = Settings.Default.nRunGA.ToString();
            lblResultFinal.Text = Settings.Default.ResultFinal;
        }

        #endregion ExecuteFinal

        #endregion ProgramDesign

        //------------
        ////----------////---------------------------------------------------------------------// Begin MainForm_Load
        //------------

        private void MainForm_Load(object sender, EventArgs e)
        {
            Form Info = new InfoForm();
            Info.Show();
            Info.Close();
            MPG_WinShutDowm.ShowShutForm sf = new MPG_WinShutDowm.ShowShutForm(new InfoForm());

            LoadTransactionType();
            LoadExecuteFinal();
        }

        //------------
        ////----------////---------------------------------------------------------------------// End MainForm_Load
        //------------
    }
}

