﻿namespace _8_Queens_by_Genetic_Algorithm
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnExit = new System.Windows.Forms.ToolStripButton();
            this.btnMinimized = new System.Windows.Forms.ToolStripButton();
            this.btnInfo = new System.Windows.Forms.ToolStripButton();
            this.toolStripLogo = new System.Windows.Forms.ToolStrip();
            this.lblLogo = new System.Windows.Forms.ToolStripLabel();
            this.lblNameProduction = new System.Windows.Forms.ToolStripLabel();
            this.panelMain = new System.Windows.Forms.Panel();
            this.panelDetailsProblem = new System.Windows.Forms.Panel();
            this.grbDetailsProblem = new System.Windows.Forms.GroupBox();
            this.tlpDetailsProblem = new System.Windows.Forms.TableLayoutPanel();
            this.grbNumberChromosomes = new System.Windows.Forms.GroupBox();
            this.intNumberChromosomes = new DevComponents.Editors.IntegerInput();
            this.grbLimitedCrossOver = new System.Windows.Forms.GroupBox();
            this.intLimitedCrossOver = new DevComponents.Editors.IntegerInput();
            this.grbPercentCrossOver = new System.Windows.Forms.GroupBox();
            this.lblPercentCrossOver = new System.Windows.Forms.Label();
            this.intPercentCrossOver = new DevComponents.Editors.IntegerInput();
            this.grbPercentMutation = new System.Windows.Forms.GroupBox();
            this.lblPercentMutation = new System.Windows.Forms.Label();
            this.intPercentMutation = new DevComponents.Editors.IntegerInput();
            this.btnResultNewGene = new System.Windows.Forms.Button();
            this.btnCancelCreate = new System.Windows.Forms.Button();
            this.panelTransactionType = new System.Windows.Forms.Panel();
            this.grbTransactionType = new System.Windows.Forms.GroupBox();
            this.tlpTransactionType = new System.Windows.Forms.TableLayoutPanel();
            this.rdbtnCreate = new System.Windows.Forms.RadioButton();
            this.rdbtnShow = new System.Windows.Forms.RadioButton();
            this.cmbTreeSelectionGene = new DevComponents.DotNetBar.Controls.ComboTree();
            this.imageListTableImages55 = new System.Windows.Forms.ImageList(this.components);
            this.btnCancelShow = new System.Windows.Forms.Button();
            this.panelExecuteFinal = new System.Windows.Forms.Panel();
            this.grbExecuteFinal = new System.Windows.Forms.GroupBox();
            this.tlpRows = new System.Windows.Forms.TableLayoutPanel();
            this.lblRow8 = new System.Windows.Forms.Label();
            this.lblRow7 = new System.Windows.Forms.Label();
            this.lblRow6 = new System.Windows.Forms.Label();
            this.lblRow5 = new System.Windows.Forms.Label();
            this.lblRow4 = new System.Windows.Forms.Label();
            this.lblRow3 = new System.Windows.Forms.Label();
            this.lblRow2 = new System.Windows.Forms.Label();
            this.lblRow1 = new System.Windows.Forms.Label();
            this.tlpColumns = new System.Windows.Forms.TableLayoutPanel();
            this.panelQueen8 = new System.Windows.Forms.Panel();
            this.panelQueen5 = new System.Windows.Forms.Panel();
            this.panelQueen2 = new System.Windows.Forms.Panel();
            this.lblColumn3 = new System.Windows.Forms.Label();
            this.lblColumn2 = new System.Windows.Forms.Label();
            this.lblColumn1 = new System.Windows.Forms.Label();
            this.lblColumn8 = new System.Windows.Forms.Label();
            this.lblColumn7 = new System.Windows.Forms.Label();
            this.lblColumn6 = new System.Windows.Forms.Label();
            this.lblColumn5 = new System.Windows.Forms.Label();
            this.lblColumn4 = new System.Windows.Forms.Label();
            this.panelQueen1 = new System.Windows.Forms.Panel();
            this.panelQueen3 = new System.Windows.Forms.Panel();
            this.panelQueen6 = new System.Windows.Forms.Panel();
            this.panelQueen4 = new System.Windows.Forms.Panel();
            this.panelQueen7 = new System.Windows.Forms.Panel();
            this.panelResultValue = new System.Windows.Forms.Panel();
            this.lblnRunGA = new System.Windows.Forms.Label();
            this.panelResultValueIcon = new System.Windows.Forms.Panel();
            this.lblResultFinal = new System.Windows.Forms.Label();
            this.panelChessPage = new System.Windows.Forms.Panel();
            this.tlpChessPage = new System.Windows.Forms.TableLayoutPanel();
            this.toolStripMenu.SuspendLayout();
            this.toolStripLogo.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.panelDetailsProblem.SuspendLayout();
            this.grbDetailsProblem.SuspendLayout();
            this.tlpDetailsProblem.SuspendLayout();
            this.grbNumberChromosomes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intNumberChromosomes)).BeginInit();
            this.grbLimitedCrossOver.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intLimitedCrossOver)).BeginInit();
            this.grbPercentCrossOver.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intPercentCrossOver)).BeginInit();
            this.grbPercentMutation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intPercentMutation)).BeginInit();
            this.panelTransactionType.SuspendLayout();
            this.grbTransactionType.SuspendLayout();
            this.tlpTransactionType.SuspendLayout();
            this.panelExecuteFinal.SuspendLayout();
            this.grbExecuteFinal.SuspendLayout();
            this.tlpRows.SuspendLayout();
            this.tlpColumns.SuspendLayout();
            this.panelResultValue.SuspendLayout();
            this.panelChessPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripMenu
            // 
            this.toolStripMenu.BackColor = System.Drawing.Color.Transparent;
            this.toolStripMenu.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.toolStripMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnExit,
            this.btnMinimized,
            this.btnInfo});
            this.toolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStripMenu.Name = "toolStripMenu";
            this.toolStripMenu.Size = new System.Drawing.Size(934, 25);
            this.toolStripMenu.TabIndex = 4;
            this.toolStripMenu.Text = "toolStrip1";
            // 
            // btnExit
            // 
            this.btnExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnExit.Image = global::_8_Queens_by_Genetic_Algorithm.Properties.Resources.btnExit;
            this.btnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(23, 22);
            this.btnExit.Text = "خروج";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnMinimized
            // 
            this.btnMinimized.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMinimized.Image = global::_8_Queens_by_Genetic_Algorithm.Properties.Resources.btnMinimized;
            this.btnMinimized.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMinimized.Name = "btnMinimized";
            this.btnMinimized.Size = new System.Drawing.Size(23, 22);
            this.btnMinimized.Text = "کمینه";
            this.btnMinimized.Click += new System.EventHandler(this.btnMinimized_Click);
            // 
            // btnInfo
            // 
            this.btnInfo.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnInfo.Image = global::_8_Queens_by_Genetic_Algorithm.Properties.Resources.btnInfo;
            this.btnInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(23, 22);
            this.btnInfo.Text = "درباره ما";
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // toolStripLogo
            // 
            this.toolStripLogo.BackColor = System.Drawing.Color.Transparent;
            this.toolStripLogo.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.toolStripLogo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripLogo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblLogo,
            this.lblNameProduction});
            this.toolStripLogo.Location = new System.Drawing.Point(0, 25);
            this.toolStripLogo.Name = "toolStripLogo";
            this.toolStripLogo.Size = new System.Drawing.Size(934, 53);
            this.toolStripLogo.TabIndex = 5;
            this.toolStripLogo.Text = "toolStrip2";
            this.toolStripLogo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDragForm_MouseDown);
            this.toolStripLogo.MouseHover += new System.EventHandler(this.toolStripLogo_MouseHover);
            this.toolStripLogo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDragForm_MouseMove);
            // 
            // lblLogo
            // 
            this.lblLogo.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblLogo.AutoSize = false;
            this.lblLogo.BackgroundImage = global::_8_Queens_by_Genetic_Algorithm.Properties.Resources.lblLogo;
            this.lblLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.lblLogo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.lblLogo.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblLogo.ForeColor = System.Drawing.Color.Red;
            this.lblLogo.Name = "lblLogo";
            this.lblLogo.Size = new System.Drawing.Size(50, 50);
            this.lblLogo.Text = "لوگو";
            this.lblLogo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseDown);
            this.lblLogo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseMove);
            // 
            // lblNameProduction
            // 
            this.lblNameProduction.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblNameProduction.Font = new System.Drawing.Font("B Nazanin", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblNameProduction.ForeColor = System.Drawing.Color.Red;
            this.lblNameProduction.Name = "lblNameProduction";
            this.lblNameProduction.Size = new System.Drawing.Size(401, 50);
            this.lblNameProduction.Text = "حل مسئله هشت وزیر به کمک ژنتیک";
            this.lblNameProduction.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseDown);
            this.lblNameProduction.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseMove);
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.DarkOrchid;
            this.panelMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelMain.Controls.Add(this.panelDetailsProblem);
            this.panelMain.Controls.Add(this.panelTransactionType);
            this.panelMain.Controls.Add(this.panelExecuteFinal);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelMain.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.panelMain.Location = new System.Drawing.Point(0, 78);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(934, 436);
            this.panelMain.TabIndex = 6;
            // 
            // panelDetailsProblem
            // 
            this.panelDetailsProblem.BackColor = System.Drawing.Color.LightGreen;
            this.panelDetailsProblem.Controls.Add(this.grbDetailsProblem);
            this.panelDetailsProblem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDetailsProblem.Location = new System.Drawing.Point(394, 100);
            this.panelDetailsProblem.Name = "panelDetailsProblem";
            this.panelDetailsProblem.Size = new System.Drawing.Size(536, 332);
            this.panelDetailsProblem.TabIndex = 2;
            // 
            // grbDetailsProblem
            // 
            this.grbDetailsProblem.BackColor = System.Drawing.Color.LimeGreen;
            this.grbDetailsProblem.Controls.Add(this.tlpDetailsProblem);
            this.grbDetailsProblem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbDetailsProblem.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbDetailsProblem.ForeColor = System.Drawing.Color.Crimson;
            this.grbDetailsProblem.Location = new System.Drawing.Point(0, 0);
            this.grbDetailsProblem.Name = "grbDetailsProblem";
            this.grbDetailsProblem.Size = new System.Drawing.Size(536, 332);
            this.grbDetailsProblem.TabIndex = 0;
            this.grbDetailsProblem.TabStop = false;
            this.grbDetailsProblem.Text = "جزییات مسئله";
            // 
            // tlpDetailsProblem
            // 
            this.tlpDetailsProblem.ColumnCount = 5;
            this.tlpDetailsProblem.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpDetailsProblem.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tlpDetailsProblem.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpDetailsProblem.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpDetailsProblem.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tlpDetailsProblem.Controls.Add(this.grbNumberChromosomes, 1, 1);
            this.tlpDetailsProblem.Controls.Add(this.grbLimitedCrossOver, 1, 2);
            this.tlpDetailsProblem.Controls.Add(this.grbPercentCrossOver, 2, 2);
            this.tlpDetailsProblem.Controls.Add(this.grbPercentMutation, 2, 3);
            this.tlpDetailsProblem.Controls.Add(this.btnResultNewGene, 3, 3);
            this.tlpDetailsProblem.Controls.Add(this.btnCancelCreate, 4, 4);
            this.tlpDetailsProblem.Location = new System.Drawing.Point(3, 30);
            this.tlpDetailsProblem.Name = "tlpDetailsProblem";
            this.tlpDetailsProblem.RowCount = 5;
            this.tlpDetailsProblem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tlpDetailsProblem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24F));
            this.tlpDetailsProblem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24F));
            this.tlpDetailsProblem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tlpDetailsProblem.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tlpDetailsProblem.Size = new System.Drawing.Size(530, 299);
            this.tlpDetailsProblem.TabIndex = 0;
            // 
            // grbNumberChromosomes
            // 
            this.grbNumberChromosomes.Controls.Add(this.intNumberChromosomes);
            this.grbNumberChromosomes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbNumberChromosomes.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbNumberChromosomes.Location = new System.Drawing.Point(321, 47);
            this.grbNumberChromosomes.Name = "grbNumberChromosomes";
            this.grbNumberChromosomes.Size = new System.Drawing.Size(153, 65);
            this.grbNumberChromosomes.TabIndex = 2;
            this.grbNumberChromosomes.TabStop = false;
            this.grbNumberChromosomes.Text = "تعداد کروموزوم‌های اولیه";
            // 
            // intNumberChromosomes
            // 
            this.intNumberChromosomes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.intNumberChromosomes.BackgroundStyle.Class = "DateTimeInputBackground";
            this.intNumberChromosomes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.intNumberChromosomes.ButtonCalculator.DisplayPosition = 2;
            this.intNumberChromosomes.ButtonClear.DisplayPosition = 1;
            this.intNumberChromosomes.ButtonClear.Visible = true;
            this.intNumberChromosomes.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.intNumberChromosomes.Increment = 50;
            this.intNumberChromosomes.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center;
            this.intNumberChromosomes.Location = new System.Drawing.Point(25, 32);
            this.intNumberChromosomes.MaxValue = 2000;
            this.intNumberChromosomes.MinValue = 0;
            this.intNumberChromosomes.Name = "intNumberChromosomes";
            this.intNumberChromosomes.ShowUpDown = true;
            this.intNumberChromosomes.Size = new System.Drawing.Size(120, 29);
            this.intNumberChromosomes.TabIndex = 3;
            this.intNumberChromosomes.Value = 100;
            this.intNumberChromosomes.WatermarkAlignment = DevComponents.Editors.eTextAlignment.Center;
            this.intNumberChromosomes.ValueChanged += new System.EventHandler(this.intNumberChromosomes_ValueChanged);
            // 
            // grbLimitedCrossOver
            // 
            this.grbLimitedCrossOver.Controls.Add(this.intLimitedCrossOver);
            this.grbLimitedCrossOver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbLimitedCrossOver.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbLimitedCrossOver.Location = new System.Drawing.Point(321, 118);
            this.grbLimitedCrossOver.Name = "grbLimitedCrossOver";
            this.grbLimitedCrossOver.Size = new System.Drawing.Size(153, 65);
            this.grbLimitedCrossOver.TabIndex = 4;
            this.grbLimitedCrossOver.TabStop = false;
            this.grbLimitedCrossOver.Text = "حداکثر تعداد تولید فرزندان";
            // 
            // intLimitedCrossOver
            // 
            this.intLimitedCrossOver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.intLimitedCrossOver.BackgroundStyle.Class = "DateTimeInputBackground";
            this.intLimitedCrossOver.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.intLimitedCrossOver.ButtonCalculator.DisplayPosition = 2;
            this.intLimitedCrossOver.ButtonClear.DisplayPosition = 1;
            this.intLimitedCrossOver.ButtonClear.Visible = true;
            this.intLimitedCrossOver.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.intLimitedCrossOver.Increment = 50;
            this.intLimitedCrossOver.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center;
            this.intLimitedCrossOver.Location = new System.Drawing.Point(25, 32);
            this.intLimitedCrossOver.MaxValue = 2000;
            this.intLimitedCrossOver.MinValue = 0;
            this.intLimitedCrossOver.Name = "intLimitedCrossOver";
            this.intLimitedCrossOver.ShowUpDown = true;
            this.intLimitedCrossOver.Size = new System.Drawing.Size(120, 29);
            this.intLimitedCrossOver.TabIndex = 3;
            this.intLimitedCrossOver.Value = 100;
            this.intLimitedCrossOver.WatermarkAlignment = DevComponents.Editors.eTextAlignment.Center;
            this.intLimitedCrossOver.ValueChanged += new System.EventHandler(this.intLimitedCrossOver_ValueChanged);
            // 
            // grbPercentCrossOver
            // 
            this.grbPercentCrossOver.Controls.Add(this.lblPercentCrossOver);
            this.grbPercentCrossOver.Controls.Add(this.intPercentCrossOver);
            this.grbPercentCrossOver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbPercentCrossOver.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbPercentCrossOver.Location = new System.Drawing.Point(189, 118);
            this.grbPercentCrossOver.Name = "grbPercentCrossOver";
            this.grbPercentCrossOver.Size = new System.Drawing.Size(126, 65);
            this.grbPercentCrossOver.TabIndex = 10;
            this.grbPercentCrossOver.TabStop = false;
            this.grbPercentCrossOver.Text = "درصد ترکیب والدها";
            // 
            // lblPercentCrossOver
            // 
            this.lblPercentCrossOver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPercentCrossOver.AutoSize = true;
            this.lblPercentCrossOver.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblPercentCrossOver.Location = new System.Drawing.Point(2, 36);
            this.lblPercentCrossOver.Name = "lblPercentCrossOver";
            this.lblPercentCrossOver.Size = new System.Drawing.Size(25, 25);
            this.lblPercentCrossOver.TabIndex = 4;
            this.lblPercentCrossOver.Text = "%";
            this.lblPercentCrossOver.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // intPercentCrossOver
            // 
            this.intPercentCrossOver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.intPercentCrossOver.BackgroundStyle.Class = "DateTimeInputBackground";
            this.intPercentCrossOver.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.intPercentCrossOver.ButtonCalculator.DisplayPosition = 2;
            this.intPercentCrossOver.ButtonClear.DisplayPosition = 1;
            this.intPercentCrossOver.ButtonClear.Visible = true;
            this.intPercentCrossOver.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.intPercentCrossOver.Increment = 2;
            this.intPercentCrossOver.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center;
            this.intPercentCrossOver.Location = new System.Drawing.Point(26, 32);
            this.intPercentCrossOver.MaxValue = 100;
            this.intPercentCrossOver.MinValue = 0;
            this.intPercentCrossOver.Name = "intPercentCrossOver";
            this.intPercentCrossOver.ShowUpDown = true;
            this.intPercentCrossOver.Size = new System.Drawing.Size(95, 29);
            this.intPercentCrossOver.TabIndex = 3;
            this.intPercentCrossOver.Value = 50;
            this.intPercentCrossOver.WatermarkAlignment = DevComponents.Editors.eTextAlignment.Center;
            this.intPercentCrossOver.ValueChanged += new System.EventHandler(this.intPercentCrossOver_ValueChanged);
            // 
            // grbPercentMutation
            // 
            this.grbPercentMutation.Controls.Add(this.lblPercentMutation);
            this.grbPercentMutation.Controls.Add(this.intPercentMutation);
            this.grbPercentMutation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbPercentMutation.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbPercentMutation.Location = new System.Drawing.Point(189, 189);
            this.grbPercentMutation.Name = "grbPercentMutation";
            this.grbPercentMutation.Size = new System.Drawing.Size(126, 59);
            this.grbPercentMutation.TabIndex = 9;
            this.grbPercentMutation.TabStop = false;
            this.grbPercentMutation.Text = "درصد جهش‌ها";
            // 
            // lblPercentMutation
            // 
            this.lblPercentMutation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPercentMutation.AutoSize = true;
            this.lblPercentMutation.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblPercentMutation.Location = new System.Drawing.Point(2, 25);
            this.lblPercentMutation.Name = "lblPercentMutation";
            this.lblPercentMutation.Size = new System.Drawing.Size(25, 25);
            this.lblPercentMutation.TabIndex = 5;
            this.lblPercentMutation.Text = "%";
            this.lblPercentMutation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // intPercentMutation
            // 
            this.intPercentMutation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.intPercentMutation.BackgroundStyle.Class = "DateTimeInputBackground";
            this.intPercentMutation.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.intPercentMutation.ButtonCalculator.DisplayPosition = 2;
            this.intPercentMutation.ButtonClear.DisplayPosition = 1;
            this.intPercentMutation.ButtonClear.Visible = true;
            this.intPercentMutation.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.intPercentMutation.Increment = 2;
            this.intPercentMutation.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center;
            this.intPercentMutation.Location = new System.Drawing.Point(26, 24);
            this.intPercentMutation.MaxValue = 100;
            this.intPercentMutation.MinValue = 0;
            this.intPercentMutation.Name = "intPercentMutation";
            this.intPercentMutation.ShowUpDown = true;
            this.intPercentMutation.Size = new System.Drawing.Size(95, 29);
            this.intPercentMutation.TabIndex = 3;
            this.intPercentMutation.Value = 50;
            this.intPercentMutation.WatermarkAlignment = DevComponents.Editors.eTextAlignment.Center;
            this.intPercentMutation.ValueChanged += new System.EventHandler(this.intPercentMutation_ValueChanged);
            // 
            // btnResultNewGene
            // 
            this.btnResultNewGene.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResultNewGene.AutoSize = true;
            this.btnResultNewGene.BackColor = System.Drawing.Color.Gold;
            this.btnResultNewGene.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnResultNewGene.Font = new System.Drawing.Font("B Titr", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnResultNewGene.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnResultNewGene.Location = new System.Drawing.Point(83, 208);
            this.btnResultNewGene.Name = "btnResultNewGene";
            this.btnResultNewGene.Size = new System.Drawing.Size(80, 40);
            this.btnResultNewGene.TabIndex = 8;
            this.btnResultNewGene.Text = "نتیجه";
            this.btnResultNewGene.UseVisualStyleBackColor = false;
            this.btnResultNewGene.Click += new System.EventHandler(this.btnResultNewGene_Click);
            // 
            // btnCancelCreate
            // 
            this.btnCancelCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelCreate.AutoSize = true;
            this.btnCancelCreate.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.btnCancelCreate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelCreate.Font = new System.Drawing.Font("B Titr", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnCancelCreate.ForeColor = System.Drawing.Color.Crimson;
            this.btnCancelCreate.Location = new System.Drawing.Point(3, 266);
            this.btnCancelCreate.Name = "btnCancelCreate";
            this.btnCancelCreate.Size = new System.Drawing.Size(48, 30);
            this.btnCancelCreate.TabIndex = 11;
            this.btnCancelCreate.Text = "انصراف";
            this.btnCancelCreate.UseVisualStyleBackColor = false;
            this.btnCancelCreate.Click += new System.EventHandler(this.btnCancelCreate_Click);
            // 
            // panelTransactionType
            // 
            this.panelTransactionType.BackColor = System.Drawing.Color.DarkOrchid;
            this.panelTransactionType.Controls.Add(this.grbTransactionType);
            this.panelTransactionType.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTransactionType.Location = new System.Drawing.Point(394, 0);
            this.panelTransactionType.Name = "panelTransactionType";
            this.panelTransactionType.Size = new System.Drawing.Size(536, 100);
            this.panelTransactionType.TabIndex = 1;
            // 
            // grbTransactionType
            // 
            this.grbTransactionType.Controls.Add(this.tlpTransactionType);
            this.grbTransactionType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbTransactionType.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbTransactionType.ForeColor = System.Drawing.Color.Cyan;
            this.grbTransactionType.Location = new System.Drawing.Point(0, 0);
            this.grbTransactionType.Name = "grbTransactionType";
            this.grbTransactionType.Size = new System.Drawing.Size(536, 100);
            this.grbTransactionType.TabIndex = 0;
            this.grbTransactionType.TabStop = false;
            this.grbTransactionType.Text = "نوع تراکنش";
            // 
            // tlpTransactionType
            // 
            this.tlpTransactionType.ColumnCount = 4;
            this.tlpTransactionType.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tlpTransactionType.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 175F));
            this.tlpTransactionType.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 175F));
            this.tlpTransactionType.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpTransactionType.Controls.Add(this.rdbtnCreate, 1, 0);
            this.tlpTransactionType.Controls.Add(this.rdbtnShow, 1, 1);
            this.tlpTransactionType.Controls.Add(this.cmbTreeSelectionGene, 2, 1);
            this.tlpTransactionType.Controls.Add(this.btnCancelShow, 3, 1);
            this.tlpTransactionType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpTransactionType.Location = new System.Drawing.Point(3, 30);
            this.tlpTransactionType.Name = "tlpTransactionType";
            this.tlpTransactionType.RowCount = 2;
            this.tlpTransactionType.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpTransactionType.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpTransactionType.Size = new System.Drawing.Size(530, 67);
            this.tlpTransactionType.TabIndex = 0;
            // 
            // rdbtnCreate
            // 
            this.rdbtnCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rdbtnCreate.AutoSize = true;
            this.rdbtnCreate.Font = new System.Drawing.Font("B Titr", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rdbtnCreate.ForeColor = System.Drawing.Color.Gold;
            this.rdbtnCreate.Location = new System.Drawing.Point(258, 3);
            this.rdbtnCreate.Name = "rdbtnCreate";
            this.rdbtnCreate.Size = new System.Drawing.Size(169, 27);
            this.rdbtnCreate.TabIndex = 0;
            this.rdbtnCreate.Text = "ایجاد";
            this.rdbtnCreate.UseVisualStyleBackColor = true;
            this.rdbtnCreate.CheckedChanged += new System.EventHandler(this.rdbtnCreate_CheckedChanged);
            // 
            // rdbtnShow
            // 
            this.rdbtnShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rdbtnShow.AutoSize = true;
            this.rdbtnShow.Font = new System.Drawing.Font("B Titr", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rdbtnShow.ForeColor = System.Drawing.Color.Red;
            this.rdbtnShow.Location = new System.Drawing.Point(258, 36);
            this.rdbtnShow.Name = "rdbtnShow";
            this.rdbtnShow.Size = new System.Drawing.Size(169, 28);
            this.rdbtnShow.TabIndex = 1;
            this.rdbtnShow.Text = "نمایش";
            this.rdbtnShow.UseVisualStyleBackColor = true;
            this.rdbtnShow.CheckedChanged += new System.EventHandler(this.rdbtnShow_CheckedChanged);
            // 
            // cmbTreeSelectionGene
            // 
            this.cmbTreeSelectionGene.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbTreeSelectionGene.BackColor = System.Drawing.SystemColors.Window;
            // 
            // 
            // 
            this.cmbTreeSelectionGene.BackgroundStyle.Class = "TextBoxBorder";
            this.cmbTreeSelectionGene.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cmbTreeSelectionGene.ButtonDropDown.Visible = true;
            this.cmbTreeSelectionGene.Font = new System.Drawing.Font("B Nazanin", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.cmbTreeSelectionGene.ImageList = this.imageListTableImages55;
            this.cmbTreeSelectionGene.Location = new System.Drawing.Point(83, 36);
            this.cmbTreeSelectionGene.Name = "cmbTreeSelectionGene";
            this.cmbTreeSelectionGene.Size = new System.Drawing.Size(169, 28);
            this.cmbTreeSelectionGene.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbTreeSelectionGene.TabIndex = 4;
            this.cmbTreeSelectionGene.WatermarkAlignment = DevComponents.Editors.eTextAlignment.Center;
            this.cmbTreeSelectionGene.WatermarkColor = System.Drawing.Color.Blue;
            this.cmbTreeSelectionGene.WatermarkFont = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.cmbTreeSelectionGene.WatermarkText = "،،، انتخاب کنید ،،،";
            this.cmbTreeSelectionGene.SelectionChanged += new DevComponents.AdvTree.AdvTreeNodeEventHandler(this.cmbTreeSelectionGene_SelectionChanged);
            // 
            // imageListTableImages55
            // 
            this.imageListTableImages55.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTableImages55.ImageStream")));
            this.imageListTableImages55.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTableImages55.Images.SetKeyName(0, "ResultValueFalse.png");
            this.imageListTableImages55.Images.SetKeyName(1, "ResultValueTrue.png");
            // 
            // btnCancelShow
            // 
            this.btnCancelShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelShow.AutoSize = true;
            this.btnCancelShow.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.btnCancelShow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelShow.Font = new System.Drawing.Font("B Titr", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnCancelShow.ForeColor = System.Drawing.Color.Crimson;
            this.btnCancelShow.Location = new System.Drawing.Point(3, 36);
            this.btnCancelShow.Name = "btnCancelShow";
            this.btnCancelShow.Size = new System.Drawing.Size(48, 28);
            this.btnCancelShow.TabIndex = 12;
            this.btnCancelShow.Text = "انصراف";
            this.btnCancelShow.UseVisualStyleBackColor = false;
            this.btnCancelShow.Click += new System.EventHandler(this.btnCancelShow_Click);
            // 
            // panelExecuteFinal
            // 
            this.panelExecuteFinal.BackColor = System.Drawing.Color.DarkKhaki;
            this.panelExecuteFinal.Controls.Add(this.grbExecuteFinal);
            this.panelExecuteFinal.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelExecuteFinal.Location = new System.Drawing.Point(0, 0);
            this.panelExecuteFinal.Name = "panelExecuteFinal";
            this.panelExecuteFinal.Size = new System.Drawing.Size(394, 432);
            this.panelExecuteFinal.TabIndex = 0;
            // 
            // grbExecuteFinal
            // 
            this.grbExecuteFinal.BackColor = System.Drawing.Color.PaleVioletRed;
            this.grbExecuteFinal.Controls.Add(this.tlpRows);
            this.grbExecuteFinal.Controls.Add(this.tlpColumns);
            this.grbExecuteFinal.Controls.Add(this.panelChessPage);
            this.grbExecuteFinal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbExecuteFinal.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbExecuteFinal.ForeColor = System.Drawing.Color.Blue;
            this.grbExecuteFinal.Location = new System.Drawing.Point(0, 0);
            this.grbExecuteFinal.Name = "grbExecuteFinal";
            this.grbExecuteFinal.Size = new System.Drawing.Size(394, 432);
            this.grbExecuteFinal.TabIndex = 0;
            this.grbExecuteFinal.TabStop = false;
            this.grbExecuteFinal.Text = "نتیجه نهایی";
            // 
            // tlpRows
            // 
            this.tlpRows.ColumnCount = 1;
            this.tlpRows.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpRows.Controls.Add(this.lblRow8, 0, 8);
            this.tlpRows.Controls.Add(this.lblRow7, 0, 7);
            this.tlpRows.Controls.Add(this.lblRow6, 0, 6);
            this.tlpRows.Controls.Add(this.lblRow5, 0, 5);
            this.tlpRows.Controls.Add(this.lblRow4, 0, 4);
            this.tlpRows.Controls.Add(this.lblRow3, 0, 3);
            this.tlpRows.Controls.Add(this.lblRow2, 0, 2);
            this.tlpRows.Controls.Add(this.lblRow1, 0, 1);
            this.tlpRows.Dock = System.Windows.Forms.DockStyle.Left;
            this.tlpRows.Location = new System.Drawing.Point(3, 89);
            this.tlpRows.Name = "tlpRows";
            this.tlpRows.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tlpRows.RowCount = 10;
            this.tlpRows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpRows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpRows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpRows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpRows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpRows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpRows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpRows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlpRows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpRows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpRows.Size = new System.Drawing.Size(41, 340);
            this.tlpRows.TabIndex = 2;
            // 
            // lblRow8
            // 
            this.lblRow8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRow8.AutoSize = true;
            this.lblRow8.Location = new System.Drawing.Point(3, 276);
            this.lblRow8.Name = "lblRow8";
            this.lblRow8.Size = new System.Drawing.Size(35, 22);
            this.lblRow8.TabIndex = 8;
            this.lblRow8.Text = "8";
            this.lblRow8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRow7
            // 
            this.lblRow7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRow7.AutoSize = true;
            this.lblRow7.Location = new System.Drawing.Point(3, 242);
            this.lblRow7.Name = "lblRow7";
            this.lblRow7.Size = new System.Drawing.Size(35, 22);
            this.lblRow7.TabIndex = 7;
            this.lblRow7.Text = "7";
            this.lblRow7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRow6
            // 
            this.lblRow6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRow6.AutoSize = true;
            this.lblRow6.Location = new System.Drawing.Point(3, 208);
            this.lblRow6.Name = "lblRow6";
            this.lblRow6.Size = new System.Drawing.Size(35, 22);
            this.lblRow6.TabIndex = 6;
            this.lblRow6.Text = "6";
            this.lblRow6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRow5
            // 
            this.lblRow5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRow5.AutoSize = true;
            this.lblRow5.Location = new System.Drawing.Point(3, 173);
            this.lblRow5.Name = "lblRow5";
            this.lblRow5.Size = new System.Drawing.Size(35, 22);
            this.lblRow5.TabIndex = 5;
            this.lblRow5.Text = "5";
            this.lblRow5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRow4
            // 
            this.lblRow4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRow4.AutoSize = true;
            this.lblRow4.Location = new System.Drawing.Point(3, 138);
            this.lblRow4.Name = "lblRow4";
            this.lblRow4.Size = new System.Drawing.Size(35, 22);
            this.lblRow4.TabIndex = 4;
            this.lblRow4.Text = "4";
            this.lblRow4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRow3
            // 
            this.lblRow3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRow3.AutoSize = true;
            this.lblRow3.Location = new System.Drawing.Point(3, 104);
            this.lblRow3.Name = "lblRow3";
            this.lblRow3.Size = new System.Drawing.Size(35, 22);
            this.lblRow3.TabIndex = 3;
            this.lblRow3.Text = "3";
            this.lblRow3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRow2
            // 
            this.lblRow2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRow2.AutoSize = true;
            this.lblRow2.Location = new System.Drawing.Point(3, 70);
            this.lblRow2.Name = "lblRow2";
            this.lblRow2.Size = new System.Drawing.Size(35, 22);
            this.lblRow2.TabIndex = 2;
            this.lblRow2.Text = "2";
            this.lblRow2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRow1
            // 
            this.lblRow1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRow1.AutoSize = true;
            this.lblRow1.Location = new System.Drawing.Point(3, 36);
            this.lblRow1.Name = "lblRow1";
            this.lblRow1.Size = new System.Drawing.Size(35, 22);
            this.lblRow1.TabIndex = 0;
            this.lblRow1.Text = "1";
            this.lblRow1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tlpColumns
            // 
            this.tlpColumns.ColumnCount = 10;
            this.tlpColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 79F));
            this.tlpColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlpColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpColumns.Controls.Add(this.panelQueen8, 8, 0);
            this.tlpColumns.Controls.Add(this.panelQueen5, 5, 0);
            this.tlpColumns.Controls.Add(this.panelQueen2, 2, 0);
            this.tlpColumns.Controls.Add(this.lblColumn3, 3, 1);
            this.tlpColumns.Controls.Add(this.lblColumn2, 2, 1);
            this.tlpColumns.Controls.Add(this.lblColumn1, 1, 1);
            this.tlpColumns.Controls.Add(this.lblColumn8, 8, 1);
            this.tlpColumns.Controls.Add(this.lblColumn7, 7, 1);
            this.tlpColumns.Controls.Add(this.lblColumn6, 6, 1);
            this.tlpColumns.Controls.Add(this.lblColumn5, 5, 1);
            this.tlpColumns.Controls.Add(this.lblColumn4, 4, 1);
            this.tlpColumns.Controls.Add(this.panelQueen1, 1, 0);
            this.tlpColumns.Controls.Add(this.panelQueen3, 3, 0);
            this.tlpColumns.Controls.Add(this.panelQueen6, 6, 0);
            this.tlpColumns.Controls.Add(this.panelQueen4, 4, 0);
            this.tlpColumns.Controls.Add(this.panelQueen7, 7, 0);
            this.tlpColumns.Controls.Add(this.panelResultValue, 0, 0);
            this.tlpColumns.Controls.Add(this.lblResultFinal, 0, 1);
            this.tlpColumns.Dock = System.Windows.Forms.DockStyle.Top;
            this.tlpColumns.Location = new System.Drawing.Point(3, 25);
            this.tlpColumns.Name = "tlpColumns";
            this.tlpColumns.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tlpColumns.RowCount = 2;
            this.tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpColumns.Size = new System.Drawing.Size(388, 64);
            this.tlpColumns.TabIndex = 1;
            // 
            // panelQueen8
            // 
            this.panelQueen8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelQueen8.BackgroundImage")));
            this.panelQueen8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen8.Location = new System.Drawing.Point(322, 3);
            this.panelQueen8.Name = "panelQueen8";
            this.panelQueen8.Size = new System.Drawing.Size(29, 29);
            this.panelQueen8.TabIndex = 12;
            // 
            // panelQueen5
            // 
            this.panelQueen5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelQueen5.BackgroundImage")));
            this.panelQueen5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen5.Location = new System.Drawing.Point(219, 3);
            this.panelQueen5.Name = "panelQueen5";
            this.panelQueen5.Size = new System.Drawing.Size(29, 29);
            this.panelQueen5.TabIndex = 12;
            // 
            // panelQueen2
            // 
            this.panelQueen2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelQueen2.BackgroundImage")));
            this.panelQueen2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen2.Location = new System.Drawing.Point(117, 3);
            this.panelQueen2.Name = "panelQueen2";
            this.panelQueen2.Size = new System.Drawing.Size(27, 29);
            this.panelQueen2.TabIndex = 11;
            // 
            // lblColumn3
            // 
            this.lblColumn3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn3.AutoSize = true;
            this.lblColumn3.Location = new System.Drawing.Point(150, 38);
            this.lblColumn3.Name = "lblColumn3";
            this.lblColumn3.Size = new System.Drawing.Size(29, 22);
            this.lblColumn3.TabIndex = 4;
            this.lblColumn3.Text = "3";
            this.lblColumn3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn2
            // 
            this.lblColumn2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn2.AutoSize = true;
            this.lblColumn2.Location = new System.Drawing.Point(117, 38);
            this.lblColumn2.Name = "lblColumn2";
            this.lblColumn2.Size = new System.Drawing.Size(27, 22);
            this.lblColumn2.TabIndex = 3;
            this.lblColumn2.Text = "2";
            this.lblColumn2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn1
            // 
            this.lblColumn1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn1.AutoSize = true;
            this.lblColumn1.Location = new System.Drawing.Point(82, 38);
            this.lblColumn1.Name = "lblColumn1";
            this.lblColumn1.Size = new System.Drawing.Size(29, 22);
            this.lblColumn1.TabIndex = 1;
            this.lblColumn1.Text = "1";
            this.lblColumn1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn8
            // 
            this.lblColumn8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn8.AutoSize = true;
            this.lblColumn8.Location = new System.Drawing.Point(322, 38);
            this.lblColumn8.Name = "lblColumn8";
            this.lblColumn8.Size = new System.Drawing.Size(29, 22);
            this.lblColumn8.TabIndex = 9;
            this.lblColumn8.Text = "8";
            this.lblColumn8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn7
            // 
            this.lblColumn7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn7.AutoSize = true;
            this.lblColumn7.Location = new System.Drawing.Point(288, 38);
            this.lblColumn7.Name = "lblColumn7";
            this.lblColumn7.Size = new System.Drawing.Size(28, 22);
            this.lblColumn7.TabIndex = 8;
            this.lblColumn7.Text = "7";
            this.lblColumn7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn6
            // 
            this.lblColumn6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn6.AutoSize = true;
            this.lblColumn6.Location = new System.Drawing.Point(254, 38);
            this.lblColumn6.Name = "lblColumn6";
            this.lblColumn6.Size = new System.Drawing.Size(28, 22);
            this.lblColumn6.TabIndex = 7;
            this.lblColumn6.Text = "6";
            this.lblColumn6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn5
            // 
            this.lblColumn5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn5.AutoSize = true;
            this.lblColumn5.Location = new System.Drawing.Point(219, 38);
            this.lblColumn5.Name = "lblColumn5";
            this.lblColumn5.Size = new System.Drawing.Size(29, 22);
            this.lblColumn5.TabIndex = 6;
            this.lblColumn5.Text = "5";
            this.lblColumn5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColumn4
            // 
            this.lblColumn4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn4.AutoSize = true;
            this.lblColumn4.Location = new System.Drawing.Point(185, 38);
            this.lblColumn4.Name = "lblColumn4";
            this.lblColumn4.Size = new System.Drawing.Size(28, 22);
            this.lblColumn4.TabIndex = 5;
            this.lblColumn4.Text = "4";
            this.lblColumn4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelQueen1
            // 
            this.panelQueen1.BackgroundImage = global::_8_Queens_by_Genetic_Algorithm.Properties.Resources.Queens;
            this.panelQueen1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen1.Location = new System.Drawing.Point(82, 3);
            this.panelQueen1.Name = "panelQueen1";
            this.panelQueen1.Size = new System.Drawing.Size(29, 29);
            this.panelQueen1.TabIndex = 10;
            // 
            // panelQueen3
            // 
            this.panelQueen3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelQueen3.BackgroundImage")));
            this.panelQueen3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen3.Location = new System.Drawing.Point(150, 3);
            this.panelQueen3.Name = "panelQueen3";
            this.panelQueen3.Size = new System.Drawing.Size(29, 29);
            this.panelQueen3.TabIndex = 11;
            // 
            // panelQueen6
            // 
            this.panelQueen6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelQueen6.BackgroundImage")));
            this.panelQueen6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen6.Location = new System.Drawing.Point(254, 3);
            this.panelQueen6.Name = "panelQueen6";
            this.panelQueen6.Size = new System.Drawing.Size(28, 29);
            this.panelQueen6.TabIndex = 12;
            // 
            // panelQueen4
            // 
            this.panelQueen4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelQueen4.BackgroundImage")));
            this.panelQueen4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen4.Location = new System.Drawing.Point(185, 3);
            this.panelQueen4.Name = "panelQueen4";
            this.panelQueen4.Size = new System.Drawing.Size(28, 29);
            this.panelQueen4.TabIndex = 12;
            // 
            // panelQueen7
            // 
            this.panelQueen7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelQueen7.BackgroundImage")));
            this.panelQueen7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelQueen7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelQueen7.Location = new System.Drawing.Point(288, 3);
            this.panelQueen7.Name = "panelQueen7";
            this.panelQueen7.Size = new System.Drawing.Size(28, 29);
            this.panelQueen7.TabIndex = 13;
            // 
            // panelResultValue
            // 
            this.panelResultValue.Controls.Add(this.lblnRunGA);
            this.panelResultValue.Controls.Add(this.panelResultValueIcon);
            this.panelResultValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelResultValue.Location = new System.Drawing.Point(3, 3);
            this.panelResultValue.Name = "panelResultValue";
            this.panelResultValue.Size = new System.Drawing.Size(73, 29);
            this.panelResultValue.TabIndex = 15;
            // 
            // lblnRunGA
            // 
            this.lblnRunGA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblnRunGA.AutoSize = true;
            this.lblnRunGA.Location = new System.Drawing.Point(36, 5);
            this.lblnRunGA.Name = "lblnRunGA";
            this.lblnRunGA.Size = new System.Drawing.Size(14, 22);
            this.lblnRunGA.TabIndex = 15;
            this.lblnRunGA.Text = "-";
            this.lblnRunGA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelResultValueIcon
            // 
            this.panelResultValueIcon.BackgroundImage = global::_8_Queens_by_Genetic_Algorithm.Properties.Resources.ResultValueFalse;
            this.panelResultValueIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelResultValueIcon.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelResultValueIcon.Location = new System.Drawing.Point(0, 0);
            this.panelResultValueIcon.Name = "panelResultValueIcon";
            this.panelResultValueIcon.Size = new System.Drawing.Size(29, 29);
            this.panelResultValueIcon.TabIndex = 14;
            // 
            // lblResultFinal
            // 
            this.lblResultFinal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblResultFinal.AutoSize = true;
            this.lblResultFinal.Location = new System.Drawing.Point(3, 38);
            this.lblResultFinal.Name = "lblResultFinal";
            this.lblResultFinal.Size = new System.Drawing.Size(73, 22);
            this.lblResultFinal.TabIndex = 16;
            this.lblResultFinal.Text = "-";
            this.lblResultFinal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelChessPage
            // 
            this.panelChessPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panelChessPage.BackgroundImage = global::_8_Queens_by_Genetic_Algorithm.Properties.Resources.ChessPage;
            this.panelChessPage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelChessPage.Controls.Add(this.tlpChessPage);
            this.panelChessPage.Location = new System.Drawing.Point(48, 86);
            this.panelChessPage.Name = "panelChessPage";
            this.panelChessPage.Size = new System.Drawing.Size(340, 340);
            this.panelChessPage.TabIndex = 0;
            // 
            // tlpChessPage
            // 
            this.tlpChessPage.BackColor = System.Drawing.Color.Transparent;
            this.tlpChessPage.ColumnCount = 10;
            this.tlpChessPage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlpChessPage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpChessPage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpChessPage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpChessPage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpChessPage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpChessPage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpChessPage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpChessPage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpChessPage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpChessPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpChessPage.Location = new System.Drawing.Point(0, 0);
            this.tlpChessPage.Name = "tlpChessPage";
            this.tlpChessPage.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tlpChessPage.RowCount = 10;
            this.tlpChessPage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tlpChessPage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpChessPage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpChessPage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpChessPage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpChessPage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpChessPage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlpChessPage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpChessPage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tlpChessPage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpChessPage.Size = new System.Drawing.Size(340, 340);
            this.tlpChessPage.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(934, 514);
            this.ControlBox = false;
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.toolStripLogo);
            this.Controls.Add(this.toolStripMenu);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.toolStripMenu.ResumeLayout(false);
            this.toolStripMenu.PerformLayout();
            this.toolStripLogo.ResumeLayout(false);
            this.toolStripLogo.PerformLayout();
            this.panelMain.ResumeLayout(false);
            this.panelDetailsProblem.ResumeLayout(false);
            this.grbDetailsProblem.ResumeLayout(false);
            this.tlpDetailsProblem.ResumeLayout(false);
            this.tlpDetailsProblem.PerformLayout();
            this.grbNumberChromosomes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.intNumberChromosomes)).EndInit();
            this.grbLimitedCrossOver.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.intLimitedCrossOver)).EndInit();
            this.grbPercentCrossOver.ResumeLayout(false);
            this.grbPercentCrossOver.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intPercentCrossOver)).EndInit();
            this.grbPercentMutation.ResumeLayout(false);
            this.grbPercentMutation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.intPercentMutation)).EndInit();
            this.panelTransactionType.ResumeLayout(false);
            this.grbTransactionType.ResumeLayout(false);
            this.tlpTransactionType.ResumeLayout(false);
            this.tlpTransactionType.PerformLayout();
            this.panelExecuteFinal.ResumeLayout(false);
            this.grbExecuteFinal.ResumeLayout(false);
            this.tlpRows.ResumeLayout(false);
            this.tlpRows.PerformLayout();
            this.tlpColumns.ResumeLayout(false);
            this.tlpColumns.PerformLayout();
            this.panelResultValue.ResumeLayout(false);
            this.panelResultValue.PerformLayout();
            this.panelChessPage.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripMenu;
        private System.Windows.Forms.ToolStripButton btnExit;
        private System.Windows.Forms.ToolStripButton btnMinimized;
        private System.Windows.Forms.ToolStripButton btnInfo;
        private System.Windows.Forms.ToolStrip toolStripLogo;
        private System.Windows.Forms.ToolStripLabel lblLogo;
        private System.Windows.Forms.ToolStripLabel lblNameProduction;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Panel panelDetailsProblem;
        private System.Windows.Forms.GroupBox grbDetailsProblem;
        private System.Windows.Forms.TableLayoutPanel tlpDetailsProblem;
        private System.Windows.Forms.GroupBox grbNumberChromosomes;
        private System.Windows.Forms.Button btnResultNewGene;
        private System.Windows.Forms.Panel panelTransactionType;
        private System.Windows.Forms.GroupBox grbTransactionType;
        private System.Windows.Forms.TableLayoutPanel tlpTransactionType;
        private System.Windows.Forms.RadioButton rdbtnCreate;
        private System.Windows.Forms.RadioButton rdbtnShow;
        private System.Windows.Forms.Panel panelExecuteFinal;
        private System.Windows.Forms.GroupBox grbExecuteFinal;
        private System.Windows.Forms.TableLayoutPanel tlpRows;
        private System.Windows.Forms.Label lblRow8;
        private System.Windows.Forms.Label lblRow7;
        private System.Windows.Forms.Label lblRow6;
        private System.Windows.Forms.Label lblRow5;
        private System.Windows.Forms.Label lblRow4;
        private System.Windows.Forms.Label lblRow3;
        private System.Windows.Forms.Label lblRow2;
        private System.Windows.Forms.Label lblRow1;
        private System.Windows.Forms.TableLayoutPanel tlpColumns;
        private System.Windows.Forms.Label lblColumn3;
        private System.Windows.Forms.Label lblColumn2;
        private System.Windows.Forms.Label lblColumn1;
        private System.Windows.Forms.Label lblColumn8;
        private System.Windows.Forms.Label lblColumn7;
        private System.Windows.Forms.Label lblColumn6;
        private System.Windows.Forms.Label lblColumn5;
        private System.Windows.Forms.Label lblColumn4;
        private System.Windows.Forms.Panel panelChessPage;
        private System.Windows.Forms.TableLayoutPanel tlpChessPage;
        private DevComponents.DotNetBar.Controls.ComboTree cmbTreeSelectionGene;
        private DevComponents.Editors.IntegerInput intNumberChromosomes;
        private System.Windows.Forms.Panel panelQueen8;
        private System.Windows.Forms.Panel panelQueen5;
        private System.Windows.Forms.Panel panelQueen2;
        private System.Windows.Forms.Panel panelQueen1;
        private System.Windows.Forms.Panel panelQueen3;
        private System.Windows.Forms.Panel panelQueen6;
        private System.Windows.Forms.Panel panelQueen4;
        private System.Windows.Forms.Panel panelQueen7;
        private System.Windows.Forms.GroupBox grbPercentMutation;
        private DevComponents.Editors.IntegerInput intPercentMutation;
        private System.Windows.Forms.GroupBox grbLimitedCrossOver;
        private DevComponents.Editors.IntegerInput intLimitedCrossOver;
        private System.Windows.Forms.GroupBox grbPercentCrossOver;
        private DevComponents.Editors.IntegerInput intPercentCrossOver;
        private System.Windows.Forms.Button btnCancelCreate;
        private System.Windows.Forms.Button btnCancelShow;
        private System.Windows.Forms.Label lblPercentCrossOver;
        private System.Windows.Forms.Label lblPercentMutation;
        private System.Windows.Forms.Panel panelResultValueIcon;
        private System.Windows.Forms.Panel panelResultValue;
        private System.Windows.Forms.Label lblnRunGA;
        private System.Windows.Forms.Label lblResultFinal;
        private System.Windows.Forms.ImageList imageListTableImages55;
    }
}

