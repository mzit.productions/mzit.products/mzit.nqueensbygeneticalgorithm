﻿namespace _8_Queens_by_Genetic_Algorithm
{
    partial class InfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InfoForm));
            this.tlpAbout = new System.Windows.Forms.TableLayoutPanel();
            this.toolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnExit = new System.Windows.Forms.ToolStripButton();
            this.tlpstiAboutDetails = new System.Windows.Forms.TableLayoutPanel();
            this.tlpAboutUs = new System.Windows.Forms.TableLayoutPanel();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.tlpANP = new System.Windows.Forms.TableLayoutPanel();
            this.tlpVersion = new System.Windows.Forms.TableLayoutPanel();
            this.panelVersion = new System.Windows.Forms.Panel();
            this.lblVersionShow = new DevComponents.DotNetBar.LabelX();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.linkLblEmail = new System.Windows.Forms.LinkLabel();
            this.lblAboutNameProduction = new System.Windows.Forms.Label();
            this.tlpAboutDescription = new System.Windows.Forms.TableLayoutPanel();
            this.grbInTheFuture = new System.Windows.Forms.GroupBox();
            this.rchtxtInTheFuture = new System.Windows.Forms.RichTextBox();
            this.grbAphorism = new System.Windows.Forms.GroupBox();
            this.rchtxtAphorism = new System.Windows.Forms.RichTextBox();
            this.tlpAbout.SuspendLayout();
            this.toolStripMenu.SuspendLayout();
            this.tlpstiAboutDetails.SuspendLayout();
            this.tlpAboutUs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.tlpANP.SuspendLayout();
            this.tlpVersion.SuspendLayout();
            this.panelVersion.SuspendLayout();
            this.tlpAboutDescription.SuspendLayout();
            this.grbInTheFuture.SuspendLayout();
            this.grbAphorism.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpAbout
            // 
            this.tlpAbout.BackColor = System.Drawing.Color.Transparent;
            this.tlpAbout.ColumnCount = 3;
            this.tlpAbout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tlpAbout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76F));
            this.tlpAbout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tlpAbout.Controls.Add(this.toolStripMenu, 0, 0);
            this.tlpAbout.Controls.Add(this.tlpstiAboutDetails, 1, 1);
            this.tlpAbout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAbout.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tlpAbout.Location = new System.Drawing.Point(0, 0);
            this.tlpAbout.Name = "tlpAbout";
            this.tlpAbout.RowCount = 3;
            this.tlpAbout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tlpAbout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 74F));
            this.tlpAbout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tlpAbout.Size = new System.Drawing.Size(784, 484);
            this.tlpAbout.TabIndex = 1;
            // 
            // toolStripMenu
            // 
            this.toolStripMenu.BackColor = System.Drawing.Color.Transparent;
            this.toolStripMenu.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.toolStripMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnExit});
            this.toolStripMenu.Location = new System.Drawing.Point(690, 0);
            this.toolStripMenu.Name = "toolStripMenu";
            this.toolStripMenu.Size = new System.Drawing.Size(94, 25);
            this.toolStripMenu.TabIndex = 2;
            this.toolStripMenu.Text = "toolStrip1";
            // 
            // btnExit
            // 
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnExit.Image = global::_8_Queens_by_Genetic_Algorithm.Properties.Resources.btnExit;
            this.btnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(23, 22);
            this.btnExit.Text = "خروج";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // tlpstiAboutDetails
            // 
            this.tlpstiAboutDetails.BackColor = System.Drawing.Color.SteelBlue;
            this.tlpstiAboutDetails.ColumnCount = 1;
            this.tlpstiAboutDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpstiAboutDetails.Controls.Add(this.tlpAboutUs, 0, 0);
            this.tlpstiAboutDetails.Controls.Add(this.tlpAboutDescription, 0, 2);
            this.tlpstiAboutDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpstiAboutDetails.Location = new System.Drawing.Point(98, 65);
            this.tlpstiAboutDetails.Name = "tlpstiAboutDetails";
            this.tlpstiAboutDetails.RowCount = 3;
            this.tlpstiAboutDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46F));
            this.tlpstiAboutDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpstiAboutDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44F));
            this.tlpstiAboutDetails.Size = new System.Drawing.Size(589, 352);
            this.tlpstiAboutDetails.TabIndex = 0;
            // 
            // tlpAboutUs
            // 
            this.tlpAboutUs.BackColor = System.Drawing.Color.SpringGreen;
            this.tlpAboutUs.ColumnCount = 3;
            this.tlpAboutUs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpAboutUs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tlpAboutUs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tlpAboutUs.Controls.Add(this.picLogo, 0, 0);
            this.tlpAboutUs.Controls.Add(this.tlpANP, 2, 0);
            this.tlpAboutUs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAboutUs.Location = new System.Drawing.Point(3, 3);
            this.tlpAboutUs.Name = "tlpAboutUs";
            this.tlpAboutUs.RowCount = 1;
            this.tlpAboutUs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAboutUs.Size = new System.Drawing.Size(583, 155);
            this.tlpAboutUs.TabIndex = 1;
            // 
            // picLogo
            // 
            this.picLogo.BackColor = System.Drawing.Color.Yellow;
            this.picLogo.BackgroundImage = global::_8_Queens_by_Genetic_Algorithm.Properties.Resources.lblLogo;
            this.picLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLogo.Location = new System.Drawing.Point(456, 3);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(124, 149);
            this.picLogo.TabIndex = 0;
            this.picLogo.TabStop = false;
            // 
            // tlpANP
            // 
            this.tlpANP.ColumnCount = 1;
            this.tlpANP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpANP.Controls.Add(this.tlpVersion, 0, 1);
            this.tlpANP.Controls.Add(this.lblAboutNameProduction, 0, 0);
            this.tlpANP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpANP.Location = new System.Drawing.Point(3, 3);
            this.tlpANP.Name = "tlpANP";
            this.tlpANP.RowCount = 2;
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tlpANP.Size = new System.Drawing.Size(387, 149);
            this.tlpANP.TabIndex = 1;
            // 
            // tlpVersion
            // 
            this.tlpVersion.ColumnCount = 4;
            this.tlpVersion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpVersion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tlpVersion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpVersion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpVersion.Controls.Add(this.panelVersion, 2, 0);
            this.tlpVersion.Controls.Add(this.lblVersion, 1, 0);
            this.tlpVersion.Controls.Add(this.lblEmail, 1, 1);
            this.tlpVersion.Controls.Add(this.linkLblEmail, 2, 1);
            this.tlpVersion.Font = new System.Drawing.Font("B Nazanin", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tlpVersion.Location = new System.Drawing.Point(3, 62);
            this.tlpVersion.Name = "tlpVersion";
            this.tlpVersion.RowCount = 2;
            this.tlpVersion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpVersion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpVersion.Size = new System.Drawing.Size(381, 84);
            this.tlpVersion.TabIndex = 2;
            // 
            // panelVersion
            // 
            this.panelVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panelVersion.Controls.Add(this.lblVersionShow);
            this.panelVersion.Location = new System.Drawing.Point(23, 5);
            this.panelVersion.Name = "panelVersion";
            this.panelVersion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panelVersion.Size = new System.Drawing.Size(235, 32);
            this.panelVersion.TabIndex = 5;
            // 
            // lblVersionShow
            // 
            this.lblVersionShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.lblVersionShow.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblVersionShow.Font = new System.Drawing.Font("B Nazanin", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblVersionShow.Location = new System.Drawing.Point(3, 4);
            this.lblVersionShow.Name = "lblVersionShow";
            this.lblVersionShow.Size = new System.Drawing.Size(228, 24);
            this.lblVersionShow.TabIndex = 3;
            this.lblVersionShow.Text = "۱۴.۱۲";
            this.lblVersionShow.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(264, 9);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(94, 24);
            this.lblVersion.TabIndex = 1;
            this.lblVersion.Text = "نسخه:";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEmail
            // 
            this.lblEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(264, 51);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(94, 24);
            this.lblEmail.TabIndex = 3;
            this.lblEmail.Text = "ایمیل سازنده:";
            this.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // linkLblEmail
            // 
            this.linkLblEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLblEmail.AutoSize = true;
            this.linkLblEmail.Location = new System.Drawing.Point(23, 51);
            this.linkLblEmail.Name = "linkLblEmail";
            this.linkLblEmail.Size = new System.Drawing.Size(235, 24);
            this.linkLblEmail.TabIndex = 4;
            this.linkLblEmail.TabStop = true;
            this.linkLblEmail.Text = "MehdiMzITProductions@GMail.com";
            this.linkLblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAboutNameProduction
            // 
            this.lblAboutNameProduction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAboutNameProduction.AutoSize = true;
            this.lblAboutNameProduction.Font = new System.Drawing.Font("B Nazanin", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblAboutNameProduction.ForeColor = System.Drawing.Color.Red;
            this.lblAboutNameProduction.Location = new System.Drawing.Point(3, 8);
            this.lblAboutNameProduction.Name = "lblAboutNameProduction";
            this.lblAboutNameProduction.Size = new System.Drawing.Size(381, 43);
            this.lblAboutNameProduction.TabIndex = 1;
            this.lblAboutNameProduction.Text = "حل مسئله هشت وزیر به کمک ژنتیک";
            // 
            // tlpAboutDescription
            // 
            this.tlpAboutDescription.ColumnCount = 3;
            this.tlpAboutDescription.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAboutDescription.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpAboutDescription.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAboutDescription.Controls.Add(this.grbInTheFuture, 2, 0);
            this.tlpAboutDescription.Controls.Add(this.grbAphorism, 0, 0);
            this.tlpAboutDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAboutDescription.Location = new System.Drawing.Point(3, 199);
            this.tlpAboutDescription.Name = "tlpAboutDescription";
            this.tlpAboutDescription.RowCount = 1;
            this.tlpAboutDescription.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAboutDescription.Size = new System.Drawing.Size(583, 150);
            this.tlpAboutDescription.TabIndex = 2;
            // 
            // grbInTheFuture
            // 
            this.grbInTheFuture.Controls.Add(this.rchtxtInTheFuture);
            this.grbInTheFuture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbInTheFuture.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbInTheFuture.Location = new System.Drawing.Point(3, 3);
            this.grbInTheFuture.Name = "grbInTheFuture";
            this.grbInTheFuture.Size = new System.Drawing.Size(276, 144);
            this.grbInTheFuture.TabIndex = 2;
            this.grbInTheFuture.TabStop = false;
            this.grbInTheFuture.Text = "ویژگی‌های برنامه";
            // 
            // rchtxtInTheFuture
            // 
            this.rchtxtInTheFuture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rchtxtInTheFuture.ForeColor = System.Drawing.Color.MidnightBlue;
            this.rchtxtInTheFuture.Location = new System.Drawing.Point(3, 25);
            this.rchtxtInTheFuture.Name = "rchtxtInTheFuture";
            this.rchtxtInTheFuture.ReadOnly = true;
            this.rchtxtInTheFuture.ShowSelectionMargin = true;
            this.rchtxtInTheFuture.Size = new System.Drawing.Size(270, 116);
            this.rchtxtInTheFuture.TabIndex = 0;
            this.rchtxtInTheFuture.Text = resources.GetString("rchtxtInTheFuture.Text");
            // 
            // grbAphorism
            // 
            this.grbAphorism.Controls.Add(this.rchtxtAphorism);
            this.grbAphorism.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbAphorism.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbAphorism.Location = new System.Drawing.Point(305, 3);
            this.grbAphorism.Name = "grbAphorism";
            this.grbAphorism.Size = new System.Drawing.Size(275, 144);
            this.grbAphorism.TabIndex = 1;
            this.grbAphorism.TabStop = false;
            this.grbAphorism.Text = "سخنی کوتاه";
            // 
            // rchtxtAphorism
            // 
            this.rchtxtAphorism.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rchtxtAphorism.ForeColor = System.Drawing.Color.MidnightBlue;
            this.rchtxtAphorism.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rchtxtAphorism.Location = new System.Drawing.Point(3, 25);
            this.rchtxtAphorism.Name = "rchtxtAphorism";
            this.rchtxtAphorism.ReadOnly = true;
            this.rchtxtAphorism.ShowSelectionMargin = true;
            this.rchtxtAphorism.Size = new System.Drawing.Size(269, 116);
            this.rchtxtAphorism.TabIndex = 0;
            this.rchtxtAphorism.Text = "با سلام خدمت کاربران\n\nدر این برنامه سعی شده است که مسئله‌ی 8 وزیر به کمک الگوریتم" +
    " ژنتیک حل شود.\nاکثر ایرادهای منطقی و خطاهای رایج این برنامه گرفته شده است.\n\nبا ت" +
    "شکر\n\nMehdi MzIT";
            // 
            // InfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Turquoise;
            this.ClientSize = new System.Drawing.Size(784, 484);
            this.ControlBox = false;
            this.Controls.Add(this.tlpAbout);
            this.Name = "InfoForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.InfoForm_Load);
            this.tlpAbout.ResumeLayout(false);
            this.tlpAbout.PerformLayout();
            this.toolStripMenu.ResumeLayout(false);
            this.toolStripMenu.PerformLayout();
            this.tlpstiAboutDetails.ResumeLayout(false);
            this.tlpAboutUs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.tlpANP.ResumeLayout(false);
            this.tlpANP.PerformLayout();
            this.tlpVersion.ResumeLayout(false);
            this.tlpVersion.PerformLayout();
            this.panelVersion.ResumeLayout(false);
            this.tlpAboutDescription.ResumeLayout(false);
            this.grbInTheFuture.ResumeLayout(false);
            this.grbAphorism.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpAbout;
        private System.Windows.Forms.ToolStrip toolStripMenu;
        private System.Windows.Forms.ToolStripButton btnExit;
        private System.Windows.Forms.TableLayoutPanel tlpstiAboutDetails;
        private System.Windows.Forms.TableLayoutPanel tlpAboutUs;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.TableLayoutPanel tlpANP;
        private System.Windows.Forms.TableLayoutPanel tlpVersion;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.LinkLabel linkLblEmail;
        private System.Windows.Forms.Label lblAboutNameProduction;
        private System.Windows.Forms.TableLayoutPanel tlpAboutDescription;
        private System.Windows.Forms.GroupBox grbInTheFuture;
        private System.Windows.Forms.RichTextBox rchtxtInTheFuture;
        private System.Windows.Forms.GroupBox grbAphorism;
        private System.Windows.Forms.RichTextBox rchtxtAphorism;
        private System.Windows.Forms.Panel panelVersion;
        private DevComponents.DotNetBar.LabelX lblVersionShow;
    }
}